using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneradorObstaculos : MonoBehaviour
{
    public List<GameObject> obstaculoPrefabs; // Lista de prefabs de obst�culos
    public float tiempoEntreObstaculos = 2f;
    public float alturaGeneracion = 10f;
    public float radioGeneracion = 4f; // Aumenta el radio de generaci�n
    public VehiculoMovimiento jugador; // Referencia al script del jugador

    private float tiempoPasado = 0f;

    void Start()
    {
        // Obtener el componente del script VehiculoMovimiento del objeto jugador
        jugador = FindObjectOfType<VehiculoMovimiento>();

        // Comprobar si el jugador no es nulo
        if (jugador == null)
        {
            Debug.LogError("�No se encontr� el script del jugador!");
        }
    }

    void Update()
    {
        tiempoPasado += Time.deltaTime;
        if (tiempoPasado >= tiempoEntreObstaculos)
        {
            GenerarObstaculo();
            tiempoPasado = 0f;
        }
    }

    void GenerarObstaculo()
    {
        // Comprobar si la lista de prefabs de obst�culos est� vac�a
        if (obstaculoPrefabs.Count == 0)
        {
            Debug.LogError("�Lista de prefabs de obst�culos vac�a!");
            return;
        }

        // Obtener la posici�n actual del jugador
        Vector3 posicionJugador = jugador.transform.position;

        // Ajustar la posici�n del generador un poco m�s adelante del jugador (en el eje Z, por ejemplo)
        Vector3 posicionGenerador = posicionJugador + new Vector3(0f, 0f, 40f); // Ajusta el valor 10f seg�n sea necesario

        // Elegir un prefab de obst�culo aleatorio de la lista
        GameObject obstaculoPrefab = obstaculoPrefabs[Random.Range(0, obstaculoPrefabs.Count)];

        // Generar un punto aleatorio dentro del radio de generaci�n alrededor de la posici�n del generador
        Vector3 posicionAleatoria = posicionGenerador + new Vector3(Random.Range(-radioGeneracion, radioGeneracion), alturaGeneracion, Random.Range(-radioGeneracion, radioGeneracion));

        // Realizar un raycast hacia abajo desde el punto aleatorio para encontrar la carretera
        RaycastHit hit;
        if (Physics.Raycast(posicionAleatoria, Vector3.down, out hit, Mathf.Infinity))
        {
            // Si el rayo golpea algo, generar el obst�culo en la posici�n golpeada
            Instantiate(obstaculoPrefab, posicionAleatoria, Quaternion.identity);
        }
        else
        {
            // Si no golpea nada, generar el obst�culo en la posici�n aleatoria
            Instantiate(obstaculoPrefab, posicionAleatoria, Quaternion.identity);
        }
    }
}
