using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camara : MonoBehaviour
{
    public Transform target; // El objetivo que la c�mara seguir� (tu carro).
    public Vector3 offset; // El desplazamiento relativo entre la c�mara y el carro.

    void LateUpdate()
    {
        // Actualiza la posici�n de la c�mara para que siga al carro,
        // manteniendo el desplazamiento especificado.
        transform.position = target.position + target.TransformDirection(offset);

        // Hace que la c�mara mire siempre hacia el carro
        transform.LookAt(target);
    }
}
